# Radio Petrov Now Playing

is a very simple python3 script which downloads the now-playing information from
the website of the [Rádio Petrov](http://www.radiopetrov.com) (Brno's local
radio station).

## Author

Petr Cizmar (1/2018)

## Usage

`radio_petrov_now_playing [-h/--help] [-w/--watch]`

### Parameters

- `-h | --help` prints help information
- `-w | --watch` checks every minute and prints the current song if changed from last time

### Note

User can tweak the source if proxy connection is required. 

## Licence

```
Radio Petrov Now Playing
Copyright © 2018-2020 Petr Cizmar

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

```
